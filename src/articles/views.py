from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views.generic.list import  ListView
from django.views.generic.base import TemplateView
from .models import Article

def index(request):
    return HttpResponse("Hello, world. main page.")

class ArticlesList(ListView):
    model = Article
    template_name = "articles/articles.html"
    

    def get_context_data(self, **kwargs):
        context = super(ArticlesList, self).get_context_data(**kwargs)
        context['articles'] = Article.objects.all()
        return context


class ArticleDetail(TemplateView):
    template_name = "articles/articles_detail.html"

    def get_context_data(self, **kwargs):
        context = super(ArticleDetail, self).get_context_data(**kwargs)
        context['article'] = Article.objects.get(pk=kwargs['pk'])
        return context
