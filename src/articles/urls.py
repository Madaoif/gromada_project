from django.conf.urls import include, url
from . import views
from .views import ArticlesList, ArticleDetail


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'articles/$', ArticlesList.as_view(), name='articles'),
    url(r'^article/(?P<pk>\d+)/$', ArticleDetail.as_view(), name='article'),
]