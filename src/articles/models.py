# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template.defaultfilters import slugify

from django.db import models
from django.utils import timezone


def upload_location(instance, filename):
	return "%s/%s" %(instance.id, filename)

# Create your models here.
class Article(models.Model):
	title = models.CharField(max_length=250)
	slug = models.SlugField(allow_unicode=True, max_length=250)
	body = models.TextField()
	publish = models.DateTimeField(default=timezone.now)
	image=models.ImageField(upload_to=upload_location, null=True, blank=True,)

	class Meta:
		ordering = ('-publish',)

	def __unicode__(self):
		return u"%s" % self.slug

	def __str__(self):
		return self.title
